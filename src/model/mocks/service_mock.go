// Code generated by MockGen. DO NOT EDIT.
// Source: github.com/Kleiber/cart-go-template/src/model (interfaces: Model)

// Package mocks is a generated GoMock package.
package mocks

import (
	model "github.com/Kleiber/cart-go-template/src/model"
	gomock "github.com/golang/mock/gomock"
	reflect "reflect"
)

// MockModel is a mock of Model interface
type MockModel struct {
	ctrl     *gomock.Controller
	recorder *MockModelMockRecorder
}

// MockModelMockRecorder is the mock recorder for MockModel
type MockModelMockRecorder struct {
	mock *MockModel
}

// NewMockModel creates a new mock instance
func NewMockModel(ctrl *gomock.Controller) *MockModel {
	mock := &MockModel{ctrl: ctrl}
	mock.recorder = &MockModelMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockModel) EXPECT() *MockModelMockRecorder {
	return m.recorder
}

// DeleteItemCart mocks base method
func (m *MockModel) DeleteItemCart(arg0, arg1 int) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DeleteItemCart", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// DeleteItemCart indicates an expected call of DeleteItemCart
func (mr *MockModelMockRecorder) DeleteItemCart(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DeleteItemCart", reflect.TypeOf((*MockModel)(nil).DeleteItemCart), arg0, arg1)
}

// InsertCart mocks base method
func (m *MockModel) InsertCart(arg0 model.Cart) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "InsertCart", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// InsertCart indicates an expected call of InsertCart
func (mr *MockModelMockRecorder) InsertCart(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "InsertCart", reflect.TypeOf((*MockModel)(nil).InsertCart), arg0)
}

// InsertItemCart mocks base method
func (m *MockModel) InsertItemCart(arg0 int, arg1 model.Item) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "InsertItemCart", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// InsertItemCart indicates an expected call of InsertItemCart
func (mr *MockModelMockRecorder) InsertItemCart(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "InsertItemCart", reflect.TypeOf((*MockModel)(nil).InsertItemCart), arg0, arg1)
}

// ListItemsCart mocks base method
func (m *MockModel) ListItemsCart(arg0 int) ([]model.Item, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ListItemsCart", arg0)
	ret0, _ := ret[0].([]model.Item)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListItemsCart indicates an expected call of ListItemsCart
func (mr *MockModelMockRecorder) ListItemsCart(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListItemsCart", reflect.TypeOf((*MockModel)(nil).ListItemsCart), arg0)
}

// SelectCart mocks base method
func (m *MockModel) SelectCart(arg0 int) (*model.Cart, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SelectCart", arg0)
	ret0, _ := ret[0].(*model.Cart)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// SelectCart indicates an expected call of SelectCart
func (mr *MockModelMockRecorder) SelectCart(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SelectCart", reflect.TypeOf((*MockModel)(nil).SelectCart), arg0)
}

// SelectItemCart mocks base method
func (m *MockModel) SelectItemCart(arg0, arg1 int) (*model.Item, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SelectItemCart", arg0, arg1)
	ret0, _ := ret[0].(*model.Item)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// SelectItemCart indicates an expected call of SelectItemCart
func (mr *MockModelMockRecorder) SelectItemCart(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SelectItemCart", reflect.TypeOf((*MockModel)(nil).SelectItemCart), arg0, arg1)
}
