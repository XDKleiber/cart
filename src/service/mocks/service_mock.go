// Code generated by MockGen. DO NOT EDIT.
// Source: github.com/Kleiber/cart-go-template/src/service (interfaces: Service)

// Package mocks is a generated GoMock package.
package mocks

import (
	model "github.com/Kleiber/cart-go-template/src/model"
	gomock "github.com/golang/mock/gomock"
	reflect "reflect"
)

// MockService is a mock of Service interface
type MockService struct {
	ctrl     *gomock.Controller
	recorder *MockServiceMockRecorder
}

// MockServiceMockRecorder is the mock recorder for MockService
type MockServiceMockRecorder struct {
	mock *MockService
}

// NewMockService creates a new mock instance
func NewMockService(ctrl *gomock.Controller) *MockService {
	mock := &MockService{ctrl: ctrl}
	mock.recorder = &MockServiceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockService) EXPECT() *MockServiceMockRecorder {
	return m.recorder
}

// AddNewItemToCart mocks base method
func (m *MockService) AddNewItemToCart(arg0 int, arg1 model.Item) (*model.Item, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "AddNewItemToCart", arg0, arg1)
	ret0, _ := ret[0].(*model.Item)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// AddNewItemToCart indicates an expected call of AddNewItemToCart
func (mr *MockServiceMockRecorder) AddNewItemToCart(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AddNewItemToCart", reflect.TypeOf((*MockService)(nil).AddNewItemToCart), arg0, arg1)
}

// CreateNewEmptyCart mocks base method
func (m *MockService) CreateNewEmptyCart() (*model.Cart, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateNewEmptyCart")
	ret0, _ := ret[0].(*model.Cart)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateNewEmptyCart indicates an expected call of CreateNewEmptyCart
func (mr *MockServiceMockRecorder) CreateNewEmptyCart() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateNewEmptyCart", reflect.TypeOf((*MockService)(nil).CreateNewEmptyCart))
}

// GetCart mocks base method
func (m *MockService) GetCart(arg0 int) (*model.Cart, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetCart", arg0)
	ret0, _ := ret[0].(*model.Cart)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetCart indicates an expected call of GetCart
func (mr *MockServiceMockRecorder) GetCart(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetCart", reflect.TypeOf((*MockService)(nil).GetCart), arg0)
}

// RemoveItemFromCart mocks base method
func (m *MockService) RemoveItemFromCart(arg0, arg1 int) ([]model.Item, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RemoveItemFromCart", arg0, arg1)
	ret0, _ := ret[0].([]model.Item)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// RemoveItemFromCart indicates an expected call of RemoveItemFromCart
func (mr *MockServiceMockRecorder) RemoveItemFromCart(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RemoveItemFromCart", reflect.TypeOf((*MockService)(nil).RemoveItemFromCart), arg0, arg1)
}
